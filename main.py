import logging

from datetime import datetime
from httplib2 import Http

from googleapiclient import discovery
from googleapiclient.errors import HttpError
from oauth2client.client import GoogleCredentials

from flask import abort


def main(request):

    debug = request.args.get('debug')
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    # mandatory
    request_json = request.get_json(silent=True)
    if request_json is None:
        logger.error("Request body must be a json document.")
        abort(400)

    if 'project' not in request_json:
        logger.error("project is mandatory.")
        abort(400)
    project = request_json['project']
    logger.debug("using project '{}'".format(project))

    if 'instance_name' not in request_json:
        logger.error("instance_name is mandatory.")
        logger.debug("instance_name must be the Cloud SQL instance ID.")
        abort(400)
    instance_name = request_json['instance_name']
    logger.debug("using instance_name '{}'".format(instance_name))

    if 'bucket_name' not in request_json:
        logger.error("bucket_name is mandatory.")
        abort(400)
    bucket_name = request_json['bucket_name']
    logger.debug("using bucket_name '{}'".format(bucket_name))

    # set project as default value
    bucket_path = request_json.get('bucket_path', project)
    logger.debug("using bucket_path '{}'".format(bucket_path))

    # set 'sqlexport_' as default value
    file_prefix = request_json.get('file_prefix', 'sqlexport_')
    logger.debug("using file_prefix '{}'".format(file_prefix))

    if 'database' not in request_json:
        logger.error("database is mandatory.")
        logger.debug(
            """Database(s) from which the export is made. Multiple DBs can be
            defined using comma separated values.
            """
        )
        abort(400)
    database = request_json['database']
    logger.debug("using database '{}'".format(database))

    # set True as default value
    offload = request_json.get('offload', True)
    logger.debug("using default offload '{}'".format(offload))

    credentials = GoogleCredentials.get_application_default()

    service = discovery.build(
        'sqladmin',
        'v1beta4',
        http=credentials.authorize(Http()),
        cache_discovery=False
    )

    # format timestamp: YearMonthDayHourMinute
    datestamp = datetime.now().strftime("%Y%m%d%H%M")
    uri = 'gs://{}/{}/{}{}.gz'.format(bucket_name, bucket_path, file_prefix, datestamp)
    logger.debug("using uri '{}'".format(uri))

    instances_export_request_body = {
        "exportContext": {
            "kind": "sql#exportContext",
            "fileType": "SQL",
            "uri": uri,
            "databases": [database],
            "offload": offload
        }
    }

    try:
      request = service.instances().export(
            project=project,
            instance=instance_name,
            body=instances_export_request_body
      )
      response = request.execute()
      return 'OK'
    except HttpError as err:
        logging.error("Could NOT run CloudSQL export. Reason: {}".format(err))
    else:
        logging.info("CloudSQL export task status: {}".format(response))

