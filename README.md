# Cloudsql Export Cloud Function

This function makes an API call to export an instance database to a GCS bucket.

Example JSON payload to send to function:

```
{
    "project": "my-project-name",
    "instance_name": "my-sql-instance-0000000000",
    "bucket_name": "my-bucket-name",
    "bucket_path": "path-in-bucket",
    "file_prefix": "sqlexport_",
    "database": "prestashop",
    "offload": true
}
```

# Create Cloud Function

Create your Cloud Function with an Inline Editor or importing from a bucket,

configure 'main' as Entry point,

and Deploy.


# Test

In testing tab, paste the example JSON payload from above,

and replace by your own values.
